using System.Threading.Tasks;
using AccountingFarm_API.Data.Interfaces;

namespace AccountingFarm_API.Data
{
    public class AccountingFarmRepository : IControllerBase
    {
        private readonly DataContext _context;
        public AccountingFarmRepository(DataContext context)
        {
            _context = context;

        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public Task<bool> SaveAll()
        {
            throw new System.NotImplementedException();
        }
    }
}