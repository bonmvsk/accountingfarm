using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountingFarm_API.Data.Interfaces;
using AccountingFarm_API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AccountingFarm_API.Data
{
    public class GenerationRepository : IGenerationRepository
    {
        private readonly DataContext _context;
        public GenerationRepository(DataContext context)
        {
            _context = context;

        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<Generation> GetGenerationById(Guid id)
        {
            var gen = await _context.Generations.FirstOrDefaultAsync(p => p.Id == id);
            return gen;
        }

        public async Task<GenerationReport> GetGenerationReportById(Guid id)
        {
            GenerationReport report = new GenerationReport();
            var item = await _context.Items.FirstOrDefaultAsync(p => p.Type == 1 && p.Rates == 36);
            report.PriceInsurance = item.Rates;

            var costs = await _context.Costs.Include(i => i.Item).Where(p => p.GenerationId == id).ToListAsync();
            foreach (var cost in costs)
            {
                if (cost.Item.Type == 2)
                {
                    report.PriceChicken += cost.Amount * cost.Item.Rates;
                    report.ChickenLeft += cost.Amount;
                }
                else if (cost.Item.Type == 3)
                {
                    report.PriceFood += cost.Amount * cost.Item.Rates;
                }
                else if (cost.Item.Type == 4)
                {
                    report.PriceMedicine += cost.Amount * cost.Item.Rates;
                }
                else if (cost.Item.Type == 5)
                {
                    report.PriceOther += cost.Amount * cost.Item.Rates;
                }

                if (cost.Item.Type != 1)
                    report.TotalCost += cost.Amount * cost.Item.Rates;
            }

            var billTotal = await _context.Billtotals.Where(i => i.GenerationId == id).ToListAsync();
            foreach (var bill in billTotal)
            {
                report.WeightTotal += bill.WeightTotal.GetValueOrDefault(0.00m);
                report.CatchAmount += bill.Amount.GetValueOrDefault(0);
            }

            if (billTotal.Count != 0)
            {
                report.ChickenLeft = report.ChickenLeft - report.CatchAmount;
                report.Average = decimal.Round(report.WeightTotal / report.CatchAmount, 2);
                report.TotalSold = report.WeightTotal * report.PriceInsurance;
                report.Profit = report.TotalSold - report.TotalCost;
            }

            var dateout = await _context.Generations.FirstOrDefaultAsync(s => s.Id == id);
            report.DateOut = dateout.DateOut;

            return report;
        }

        public async Task<List<Generation>> GetGenerations(Guid userId)
        {
            return await _context.Generations.Where(p => p.UserId == userId).ToListAsync();
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}