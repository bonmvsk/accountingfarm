using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AccountingFarm_API.Models;

namespace AccountingFarm_API.Data.Interfaces
{
    public interface IBillDetailRepository : IControllerBase
    {
        void Add(BillDetail billDetail);
        Task<BillDetail> GetBillDetailById(Guid id);

        Task<List<BillTotal>> GetBillTotalByGenerationId(Guid id);
        Task<string> GetBillPrefix(Guid userId, Guid generationId);
    }
}