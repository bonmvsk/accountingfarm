using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AccountingFarm_API.Models;

namespace AccountingFarm_API.Data.Interfaces
{
    public interface ICostRepository : IControllerBase
    {
         Task<Cost> GetById(Guid id);
         Task<IEnumerable<Cost>> GetCosts();
         Task<IEnumerable<Cost>> GetCosts(Guid generationId);
    }
}