using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AccountingFarm_API.Models;

namespace AccountingFarm_API.Data.Interfaces
{
    public interface ICatcherRepository : IControllerBase
    {
         Task<Catcher> GetCatcher(Guid id);
         Task<IEnumerable<Catcher>> GetCatchers(Guid userId);
    }
}