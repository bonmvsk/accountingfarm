using System.Threading.Tasks;

namespace AccountingFarm_API.Data.Interfaces
{
    public interface IControllerBase
    {
         void Add<T>(T entity) where T: class;
         void Delete<T>(T entity) where T: class;
         Task<bool> SaveAll();
    }
}