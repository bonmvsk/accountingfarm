using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AccountingFarm_API.Models;

namespace AccountingFarm_API.Data.Interfaces
{
    public interface IItemRepository : IControllerBase
    {
         Task<Item> GetItem(Guid id);
         Task<IEnumerable<Item>> GetItems(Guid userId);
    }
}