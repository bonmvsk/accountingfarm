using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AccountingFarm_API.Models;

namespace AccountingFarm_API.Data.Interfaces
{
    public interface IGenerationRepository : IControllerBase
    {
         Task<List<Generation>> GetGenerations(Guid userId);
         Task<Generation> GetGenerationById(Guid id);
         Task<GenerationReport> GetGenerationReportById(Guid id);
    }
}