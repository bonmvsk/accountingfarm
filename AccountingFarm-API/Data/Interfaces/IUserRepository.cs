using System;
using System.Threading.Tasks;
using AccountingFarm_API.Helpers;
using AccountingFarm_API.Models;

namespace AccountingFarm_API.Data.Interfaces
{
    public interface IUserRepository : IControllerBase
    {
         Task<PagedList<User>> GetUsers(UserParams userParams);
         Task<User> GetUser(Guid id);
         Task<Photo> GetPhoto(int id);
         Task<Photo> GetMainPhotoForUser(Guid userId);
    }
}