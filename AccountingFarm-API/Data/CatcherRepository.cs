using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountingFarm_API.Data.Interfaces;
using AccountingFarm_API.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountingFarm_API.Data
{
    public class CatcherRepository : ICatcherRepository
    {
        private readonly DataContext _context;
        public CatcherRepository(DataContext context)
        {
            _context = context;
        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<Catcher> GetCatcher(Guid id)
        {
            var catcher = await _context.Catchers.Where(p => p.Id == id).FirstOrDefaultAsync();

            return catcher;
        }

        public async Task<IEnumerable<Catcher>> GetCatchers(Guid userId)
        {
            var list = await _context.Catchers.Where(p => p.UserId == userId).ToListAsync();

            return list;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}