using AccountingFarm_API.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountingFarm_API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<User> Users { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Cost> Costs { get; set; }
        public DbSet<CostGen> CostGen { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Generation> Generations { get; set; }
        public DbSet<BillTotal> Billtotals { get; set; }
        public DbSet<BillDetail> BillDetails { get; set; }
        public DbSet<Catcher> Catchers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BillDetail>()
                .Property<string>("WeightBasket")
                .HasField("_basket");
            modelBuilder.Entity<BillDetail>()
                .Property<string>("WeightChicken")
                .HasField("_chicken");
                
        }
    }
}