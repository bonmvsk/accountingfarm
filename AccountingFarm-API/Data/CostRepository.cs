using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountingFarm_API.Data.Interfaces;
using AccountingFarm_API.Dtos;
using AccountingFarm_API.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountingFarm_API.Data
{
    public class CostRepository : ICostRepository
    {
        private readonly DataContext _context;
        public CostRepository(DataContext context)
        {
            _context = context;
        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<Cost> GetById(Guid id)
        {
            var cost = await _context.Costs.Include(p => p.Item).FirstOrDefaultAsync(p => p.Id == id);

            return cost;
        }

        public async Task<IEnumerable<Cost>> GetCosts()
        {
            return await _context.Costs.ToListAsync();
        }

        public async Task<IEnumerable<Cost>> GetCosts(Guid generationId)
        {
            var costs = await _context.Costs.Include(p => p.Item).Where(p => p.GenerationId == generationId).ToListAsync();
            return costs;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}