using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountingFarm_API.Data.Interfaces;
using AccountingFarm_API.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountingFarm_API.Data
{
    public class ItemRepository : IItemRepository
    {
        private readonly DataContext _context;
        public ItemRepository(DataContext context)
        {
            _context = context;
        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<Item> GetItem(Guid id)
        {
            var item = await _context.Items.FirstOrDefaultAsync(p => p.Id == id);

            return item;
        }

        public async Task<IEnumerable<Item>> GetItems(Guid userId)
        {
            var items = await _context.Items.Where(p => p.UserId == userId).ToListAsync();

            return items;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}