using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountingFarm_API.Data.Interfaces;
using AccountingFarm_API.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountingFarm_API.Data
{
    public class BillDetailRepository : IBillDetailRepository
    {
        private readonly DataContext _context;
        public BillDetailRepository(DataContext context)
        {
            _context = context;

        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Add(BillDetail billDetail)
        {
            var total = new BillTotal();
            decimal chicken = 0, basket = 0;

            foreach (var item in billDetail.WeightChickens)
            {
                chicken += decimal.Parse(item);
            }
            foreach (var item in billDetail.WeightBaskets)
            {
                basket += decimal.Parse(item);
            }

            total.WeightChicken = chicken;
            total.WeightBasket = basket;
            total.WeightTotal = chicken - basket;
            total.Average = (chicken - basket) / billDetail.CatchAmount;
            total.Amount = billDetail.CatchAmount;
            total.GenerationId = billDetail.GenerationId;
            
            billDetail.BillTotal = total;
            _context.Add(billDetail);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<BillDetail> GetBillDetailById(Guid id)
        {
            var bill = await _context.BillDetails.Include(p => p.BillTotal).FirstOrDefaultAsync(p => p.Id == id);
            return bill;
        }

        public async Task<string> GetBillPrefix(Guid userId, Guid generationId)
        {
            string prefix = "";
            var date = DateTime.Now;

            var gen = await _context.Generations.Include(p => p.BillDetails).Where(p => p.UserId == userId).ToListAsync();
            var index = gen.FindIndex(p => p.Id == generationId);

            prefix = date.Year.ToString() + (index + 1).ToString("000") + (gen[index].BillDetails.Count + 1).ToString("000");
            
            return prefix;
        }

        public async Task<List<BillTotal>> GetBillTotalByGenerationId(Guid id)
        {
            var billtotal = await _context.Billtotals.Include(i => i.BillDetail).Where(p => p.GenerationId == id).ToListAsync();
            return billtotal;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}