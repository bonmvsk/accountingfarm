using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AccountingFarm_API.Data.Interfaces;
using AccountingFarm_API.Dtos;
using AccountingFarm_API.Helpers;
using AccountingFarm_API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AccountingFarm_API.Controllers
{
    [Authorize]
    [Route("api/[Controller]")]
    [ApiController]
    public class BillDetailController : ControllerBase
    {
        private readonly IBillDetailRepository _repo;
        private readonly IMapper _mapper;
        public BillDetailController(IBillDetailRepository repo, IMapper mapper)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAll(Guid id)
        {
            var billDetail = await _repo.GetBillDetailById(id);
            var billToReturn = _mapper.Map<BillDetailForReturnDto>(billDetail);
            return Ok(billToReturn);
        }

        [HttpPost]
        public async Task<IActionResult> Post(BillDetailForCreateDto billDetail)
        {
            var billForSave = _mapper.Map<BillDetail>(billDetail);

            _repo.Add(billForSave);

            if (await _repo.SaveAll())
            {

                return Created("201", null);
            }

            return BadRequest("Failed to Save the BillDetail");
        }

        [HttpGet("{id}/total")]
        public async Task<IActionResult> GetTotalAll(Guid id)
        {
            var billTotal = await _repo.GetBillTotalByGenerationId(id);
            if (billTotal != null)
            {
                var billToReturn = _mapper.Map<List<BillTotalForReturnDto>>(billTotal);
                return Ok(billToReturn);
            }
            return BadRequest($"Can't find GenerationId {id}");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, BillDetailForUpdateDto value)
        {
            var billDetail = await _repo.GetBillDetailById(id);
            if (billDetail == null)
                return BadRequest($"Can't find bill id {id}");

            _mapper.Map(value, billDetail);
            billDetail.CalBillTotal();
            if (await _repo.SaveAll())
            {
                var billToReturn = _mapper.Map<BillDetailForReturnDto>(billDetail);
                return Ok(billToReturn);
            }

            return BadRequest($"Updating bill {id} failed on save");
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteById(Guid id) 
        {
            var bill = await _repo.GetBillDetailById(id);
            if(bill != null) {
                _repo.Delete(bill);

                if (await _repo.SaveAll())
                    return Ok(true);
            }

            return BadRequest($"Deleting bill {id} failed on remove");
        }
        
        [HttpPost("prefix")]
        public async Task<IActionResult> GetPrefix([FromBody]Prefix prefix) 
        {
            var user = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if(user == null)
                return Unauthorized();

            var perfix = await _repo.GetBillPrefix(new Guid(user), prefix.GenerationId);  

            return Ok(perfix);  
        }
        
    }
}