using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AccountingFarm_API.Data.Interfaces;
using AccountingFarm_API.Dtos;
using AccountingFarm_API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AccountingFarm_API.Data.Controllers
{
    [Authorize]
    [Route("api/[Controller]")]
    [ApiController]
    public class GenerationController : ControllerBase
    {
        private readonly IGenerationRepository _repo;
        private readonly IMapper _mapper;
        private readonly IUserRepository _user;
        public GenerationController(IGenerationRepository repo, IMapper mapper, IUserRepository user)
        {
            _user = user;
            _mapper = mapper;
            _repo = repo;

        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var user = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);

            var gen = await _repo.GetGenerations(user);
            var genToReturn = _mapper.Map<List<GenerationForReturnDto>>(gen);

            return Ok(genToReturn);
        }

        [HttpPost]
        public async Task<IActionResult> Post(Generation generation)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            generation.User = _user.GetUser(Guid.Parse(userId)).Result;
            _repo.Add(generation);

            if (await _repo.SaveAll())
            {
                var genToReturn = _mapper.Map<GenerationForReturnDto>(generation);
                return Created("201", genToReturn);
            }

            return BadRequest("Failed to add the item");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, GenerationForUpdateDto generation)
        {
            var result = await _repo.GetGenerationById(id);
            if (result == null)
                return Unauthorized();

            _mapper.Map(generation, result);

            if (await _repo.SaveAll()) 
            {
                var genToReturn = _mapper.Map<GenerationForReturnDto>(result);
                return Ok(genToReturn);
            }

            return BadRequest($"Updating generation {id} failed on save");

        }

        [HttpPut("{id}/close")]
        public async Task<IActionResult> CloseGeneration(Guid id)
        {
            var result = await _repo.GetGenerationById(id);
            if (result == null)
                return Unauthorized();

            result.DateOut = DateTime.Now;
            if (await _repo.SaveAll()) 
            {
                var genToReturn = _mapper.Map<GenerationForReturnDto>(result);
                return Ok(genToReturn);
            }

            return BadRequest($"Closing generation {id} failed on save");

        }
        

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteById(Guid id)
        {
            var gen = await _repo.GetGenerationById(id);

            if (gen != null)
            {
                _repo.Delete(gen);

                if (await _repo.SaveAll())
                    return Ok();
            }

            return BadRequest("Failed to delete the Generation");
        }

        [HttpGet("{generationId}/report")]
        public async Task<IActionResult> GetGenerationReport(Guid generationId) 
        {
            var report = await _repo.GetGenerationReportById(generationId);
            return Ok(report);
        }
        
    }
}