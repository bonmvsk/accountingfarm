using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AccountingFarm_API.Data.Interfaces;
using AccountingFarm_API.Dtos;
using AccountingFarm_API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AccountingFarm_API.Controllers
{
    [Authorize]
    [Route("api/[Controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private readonly IItemRepository _repo;
        private readonly IMapper _mapper;
        public ItemController(IItemRepository repo, IMapper mapper)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet]
        public async Task<IActionResult> GetItems()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var items = await _repo.GetItems(new Guid(userId));
            var itemToReturn = _mapper.Map<IEnumerable<ItemForReturnDto>>(items);

            return Ok(itemToReturn);
        }


        [HttpGet("{id}", Name = "GetById")]
        public async Task<IActionResult> GetById(Guid id)
        {
            return Ok(await _repo.GetItem(id));
        }


        [HttpPost]
        public async Task<IActionResult> Post(Item item)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            item.UserId = new Guid(userId);
            _repo.Add(item);

            if (await _repo.SaveAll())
            {
                return CreatedAtRoute(nameof(GetById), new { id = item.Id }, item);
            }

            return BadRequest("Failed to add the item");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, ItemForUpdateDto item)
        {
            var _item = await _repo.GetItem(id);
            
            if(_item == null) 
                return Unauthorized();

            _mapper.Map(item, _item);

            if(await _repo.SaveAll()) 
                return NoContent();
            
            return BadRequest($"Updating item {id} failed on save");
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteById(Guid id)
        {
            var item = await _repo.GetItem(id);

            if (item != null)
            {
                _repo.Delete(item);

                if (await _repo.SaveAll())
                    return Ok();
            }
            return BadRequest("Failed to delete the item");
        }
    }
}