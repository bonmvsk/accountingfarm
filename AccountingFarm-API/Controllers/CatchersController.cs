using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AccountingFarm_API.Data.Interfaces;
using AccountingFarm_API.Dtos;
using AccountingFarm_API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AccountingFarm_API.Controllers
{
    [Authorize]
    [Route("api/[Controller]")]
    [ApiController]
    public class CatchersController : ControllerBase
    {
        private readonly ICatcherRepository _repo;
        private readonly IMapper _mapper;
        public CatchersController(ICatcherRepository repo, IMapper mapper)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet]
        public async Task<IActionResult> GetCatchers()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var catcher = await _repo.GetCatchers(new Guid(userId));
            var catcherToReturn = _mapper.Map<List<CatcherForReturnDto>>(catcher);

            return Ok(catcherToReturn);
        }


        [HttpGet("{id}", Name = "GetCatcherById")]
        public async Task<IActionResult> GetCatcherById(Guid id)
        {
            var catcher = await _repo.GetCatcher(id);
            var catcherToReturn = _mapper.Map<CatcherForReturnDto>(catcher); 
            return Ok(catcherToReturn);
        }


        [HttpPost]
        public async Task<IActionResult> Post(Catcher catcher)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            catcher.UserId = new Guid(userId);
            _repo.Add(catcher);

            if (await _repo.SaveAll())
            {
                return CreatedAtRoute(nameof(GetCatcherById), new { id = catcher.Id }, catcher);
            }

            return BadRequest("Failed to add the item");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, CatcherForUpdateDto catcher)
        {
            var _catcher = await _repo.GetCatcher(id);

            if (_catcher == null)
                return Unauthorized();

            _mapper.Map(catcher, _catcher);

            if (await _repo.SaveAll())
                return NoContent();

            return BadRequest($"Updating catcher {id} failed on save");
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteById(Guid id)
        {
            var catcher = await _repo.GetCatcher(id);

            if (catcher != null)
            {
                _repo.Delete(catcher);

                if (await _repo.SaveAll())
                    return Ok();
            }
            return BadRequest("Failed to delete the catcher");
        }
    }
}