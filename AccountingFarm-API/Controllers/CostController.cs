using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AccountingFarm_API.Data.Interfaces;
using AccountingFarm_API.Dtos;
using AccountingFarm_API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AccountingFarm_API.Controllers
{
    [Authorize]
    [Route("api/[Controller]")]
    [ApiController]
    public class CostController : ControllerBase
    {
        private readonly ICostRepository _repo;
        private readonly IMapper _mapper;
        public CostController(ICostRepository repo, IMapper mapper)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var costs = await _repo.GetCosts(id);
            var costsToReturn = _mapper.Map<List<CostForListDto>>(costs);
            return Ok(costsToReturn);
        }

        [HttpPost]
        public async Task<IActionResult> Post(CostForInsertDto cost)
        {
            var costToInsert = _mapper.Map<Cost>(cost);
            _repo.Add(costToInsert);

            if (await _repo.SaveAll())
            {
                var value = await _repo.GetById(costToInsert.Id);
                var costToReturn = _mapper.Map<CostForListDto>(value);
                return Created("201", costToReturn);
            }
            return BadRequest("Failed to add the cost");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, CostForUpdateDto cost)
        {
            var costToUpdate = await _repo.GetById(id);
            if(costToUpdate == null) 
                return BadRequest($"Not find Cost id {id}");
            
            _mapper.Map(cost, costToUpdate);

            if (await _repo.SaveAll())
            {
                var costToReturn = _mapper.Map<CostForListDto>(costToUpdate);
                return Ok(costToReturn);
            }
            return BadRequest("Failed to add the cost");
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteById(Guid id)
        {
            var cost = await _repo.GetById(id);

            if(cost != null) {
                _repo.Delete(cost);

                if(await _repo.SaveAll())
                    return Ok(true);
            }
            return BadRequest("Failed to delete the cost");
        }


    }
}