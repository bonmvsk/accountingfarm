using System;
using System.Linq;
using AccountingFarm_API.Dtos;
using AccountingFarm_API.Models;
using AutoMapper;

namespace AccountingFarm_API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForListDto>()
                .ForMember(dest => dest.PhotoUrl, opt =>
                {
                    opt.MapFrom(src => src.Photos.FirstOrDefault(p => p.IsMain).Url);
                })
                .ForMember(dest => dest.Age, opt =>
                {
                    opt.MapFrom(d => d.DateOfBirth.CalculateAge());
                });

            CreateMap<User, UserForDetailedDto>()
            .ForMember(dest => dest.PhotoUrl, opt =>
            {
                opt.MapFrom(src => src.Photos.FirstOrDefault(p => p.IsMain).Url);
            })
            .ForMember(dest => dest.Age, opt =>
            {
                opt.MapFrom(d => d.DateOfBirth.CalculateAge());
            });

            CreateMap<Photo, PhotosForDetailedDto>();
            CreateMap<UserForUpdateDto, User>();
            CreateMap<Photo, PhotoForReturnDto>();
            CreateMap<PhotoForCreationDto, Photo>();
            CreateMap<UserForRegisterDto, User>();
            CreateMap<GenerationForUpdateDto, Generation>();
            CreateMap<Generation, GenerationForReturnDto>()
                .ForMember(dest => dest.DateFeed, opt => 
                {
                    opt.MapFrom(src => Extensions.CalDateFeed(src.DateIn, src.DateOut));
                });
            CreateMap<BillDetailForCreateDto, BillDetail>();
            CreateMap<ItemForUpdateDto, Item>();
            CreateMap<Item, ItemForReturnDto>();
            CreateMap<CatcherForUpdateDto, Catcher>();
            CreateMap<Catcher, CatcherForReturnDto>();
            CreateMap<CostForInsertDto, Cost>();
            CreateMap<CostForUpdateDto, Cost>();
            CreateMap<Cost, CostForListDto>()
                .ForMember(dest => dest.Price, opt =>
                {
                    opt.MapFrom(src => src.Item.Rates * src.Amount);
                });
            CreateMap<BillDetailForReturnDto, BillDetail>();
            CreateMap<BillDetailForUpdateDto, BillDetail>();
            CreateMap<BillTotal, BillTotalForReturnDto>()
                .ForMember(dest => dest.Prefix, opt =>
                {
                    opt.MapFrom(src => src.BillDetail.PrefixBill);
                });
        }
    }
}