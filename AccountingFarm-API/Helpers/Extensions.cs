using System;
using AccountingFarm_API.Helpers;
using AccountingFarm_API.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AccountingFarm_API.Helpers
{
    public static class Extensions
    {
        public static void AddApplicationError(this HttpResponse response, string message)
        {
            response.Headers.Add("Application-Error", message);
            response.Headers.Add("Access-Control-Expose-Headers", "Application-Error");
            response.Headers.Add("Access-Control-Allow-Origin", "*");
        }

        public static void AddPagination(this HttpResponse response, int currentPage, int itemsPerPage, int totalItems, int totalPages)
        {
            var paginationHeader = new PaginationHeader(currentPage, itemsPerPage, totalItems, totalPages);
            var camelCaseFormatter = new JsonSerializerSettings();
            camelCaseFormatter.ContractResolver = new CamelCasePropertyNamesContractResolver();
            response.Headers.Add("Pagination", JsonConvert.SerializeObject(paginationHeader, camelCaseFormatter));
            response.Headers.Add("Access-Control-Expose-Headers", "Pagination");
        }

        public static int CalculateAge(this DateTime theDateTime)
        {
            var age = DateTime.Today.Year - theDateTime.Year;
            if (theDateTime.AddYears(age) > DateTime.Today)
                age--;

            return age;
        }

        
        public static BillDetail CalBillTotal(this BillDetail bill)
        {
            decimal chicken = 0, basket = 0;

            foreach (var item in bill.WeightChickens)
            {
                chicken += decimal.Parse(item);
            }
            foreach (var item in bill.WeightBaskets)
            {
                basket += decimal.Parse(item);
            }

            bill.BillTotal.WeightChicken = chicken;
            bill.BillTotal.WeightBasket = basket;
            bill.BillTotal.WeightTotal = chicken - basket;
            bill.BillTotal.Average = (chicken - basket) / bill.CatchAmount;
            bill.BillTotal.Amount = bill.CatchAmount;
            bill.BillTotal.GenerationId = bill.GenerationId;
            
            return bill;
        }

        public static String CalDateFeed(DateTime dateIn, DateTime? dateOut) 
        {
            if(dateOut.HasValue) {
                return (dateOut?.Date - dateIn.Date)?.Days.ToString();
            }
            return (DateTime.Now.Date - dateIn.Date).Days.ToString();
        }
    }
}