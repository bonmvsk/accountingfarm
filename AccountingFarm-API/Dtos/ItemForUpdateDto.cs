using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountingFarm_API.Dtos
{
    public class ItemForUpdateDto
    {
        [Required]
        public string Name { get; set; }
        
        [Required]
        public decimal Rates { get; set; }
    }
}