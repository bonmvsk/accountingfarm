using System;

namespace AccountingFarm_API.Dtos
{
    public class BillDetailForReturnDto
    {
        public Guid Id { get; set; }
        public string PrefixBill { get; set; }
        public DateTime CatchDate { get; set; }
        public int CatchAmount { get; set; }
        public string[] WeightChickens { get; set; }
        public string[] WeightBaskets { get; set; }
        public Guid GenerationId { get; set; }
        public Guid CatcherId { get; set; }
    }
}