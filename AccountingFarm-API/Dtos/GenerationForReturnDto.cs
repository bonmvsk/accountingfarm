using System;

namespace AccountingFarm_API.Dtos
{
    public class GenerationForReturnDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime? DateOut { get; set; }
        public string DateFeed { get; set; }
    }
}