using System;
using System.ComponentModel.DataAnnotations;
using AccountingFarm_API.Models;

namespace AccountingFarm_API.Dtos
{
    public class BillDetailForCreateDto
    {
        public string PrefixBill { get; set; }
        
        [Required]
        public DateTime CatchDate { get; set; }
        
        [Required]
        public int CatchAmount { get; set; }
        public string[] WeightChickens { get; set; }
        public string[] WeightBaskets { get; set; }

        [Required]
        public Guid GenerationId { get; set; }
        [Required]
        public Guid CatcherId { get; set; }
    }
}