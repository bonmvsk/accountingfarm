using System;
using System.ComponentModel.DataAnnotations;

namespace AccountingFarm_API.Dtos
{
    public class BillDetailForUpdateDto
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public DateTime CatchDate { get; set; }
        [Required]
        public int CatchAmount { get; set; }
        public string[] WeightChickens { get; set; }
        public string[] WeightBaskets { get; set; }
        [Required]
        public Guid CatcherId { get; set; }
    }
}