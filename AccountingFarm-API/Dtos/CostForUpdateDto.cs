using System;

namespace AccountingFarm_API.Dtos
{
    public class CostForUpdateDto
    {
        public Guid Id { get; set; }
        public int Amount { get; set; }
        public Guid ItemId { get; set; }
    }
}