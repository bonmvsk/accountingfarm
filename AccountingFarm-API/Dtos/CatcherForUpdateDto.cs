namespace AccountingFarm_API.Dtos
{
    public class CatcherForUpdateDto
    {
        public string Nickname { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string LicensePlate { get; set; }
        public string LicenseCountry { get; set; }
    }
}