using System;
using AccountingFarm_API.Models;

namespace AccountingFarm_API.Dtos
{
    public class CostForListDto
    {
        public Guid Id { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public Guid GenerationId { get; set; }
        public ItemForReturnDto Item { get; set; }
    }
}