using System;

namespace AccountingFarm_API.Dtos
{
    public class GenerationForUpdateDto
    {
        public string Name { get; set; }
        public DateTime DateIn { get; set; }
        public Nullable<DateTime> DateOut { get; set; }
    }
}