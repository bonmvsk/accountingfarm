using System;

namespace AccountingFarm_API.Dtos
{
    public class CatcherForReturnDto
    {
        public Guid Id { get; set; }
        public string Nickname { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string LicensePlate { get; set; }
        public string LicenseCountry { get; set; }
    }
}