using System;

namespace AccountingFarm_API.Dtos
{
    public class BillTotalForReturnDto
    {
        public Guid Id { get; set; }
        public string Prefix { get; set; }
        public Nullable<decimal> WeightChicken { get; set; }
        public Nullable<decimal> WeightBasket { get; set; }
        public Nullable<decimal> WeightTotal { get; set; }
        public Nullable<decimal> Average { get; set; }
        public Nullable<int> Amount { get; set; }
        public Guid BillDetailId { get; set; }
        public Guid GenerationId { get; set; }
    }
}