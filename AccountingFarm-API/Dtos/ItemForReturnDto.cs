using System;

namespace AccountingFarm_API.Dtos
{
    public class ItemForReturnDto
    {
        public Guid Id { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public decimal Rates { get; set; }
    }
}