using System;
using System.ComponentModel.DataAnnotations;

namespace AccountingFarm_API.Dtos
{
    public class CostForInsertDto
    {
        [Required]
        public int Amount { get; set; }
        
        [Required]
        public Guid ItemId { get; set; }
        
        [Required]
        public Guid GenerationId { get; set; }
    }
}