using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountingFarm_API.Models
{
    public class Cost
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public int Amount { get; set; }

        public Guid ItemId { get; set; }
        public Item Item { get; set; }

        public Guid GenerationId { get; set; }
        public Generation Generation { get; set; }
    }
}