using System;

namespace AccountingFarm_API.Models
{
    public class GenerationReport
    {
        public decimal PriceInsurance { get; set; }
        public decimal WeightTotal { get; set; }
        public int CatchAmount { get; set; }
        public decimal Average { get; set; }
        public decimal ChickenLeft { get; set; }
        public decimal PriceChicken { get; set; }
        public decimal PriceFood { get; set; }
        public decimal PriceMedicine { get; set; }
        public decimal PriceOther { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalSold { get; set; }
        public decimal Profit { get; set; }
        public DateTime? DateOut { get; set; }
    }
}