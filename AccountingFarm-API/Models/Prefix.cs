using System;

namespace AccountingFarm_API.Models
{
    public class Prefix
    {
        public string Value { get; set; }
        public Guid GenerationId { get; set; }
    }
}