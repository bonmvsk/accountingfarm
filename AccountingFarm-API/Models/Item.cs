using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountingFarm_API.Models
{
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public int Type { get; set; }
        [Required]
        public string Name { get; set; }
        public Guid UserId { get; set; }
        
        [Column(TypeName = "decimal(5, 2)")]
        public decimal Rates { get; set; }

        public Guid CostGenId { get; set; }

    }
}