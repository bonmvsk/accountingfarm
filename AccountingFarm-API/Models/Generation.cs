using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountingFarm_API.Models
{
    public class Generation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime DateIn { get; set; }
        public Nullable<DateTime> DateOut { get; set; }
        public ICollection<Cost> Costs { get; set; }
        public ICollection<BillDetail> BillDetails { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}