using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountingFarm_API.Models
{
    public class BillTotal
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        
        [Column(TypeName = "decimal(10,2)")]
        public Nullable<decimal> WeightChicken { get; set; }
        
        [Column(TypeName = "decimal(10,2)")]
        public Nullable<decimal> WeightBasket { get; set; }
        
        [Column(TypeName = "decimal(10,2)")]
        public Nullable<decimal> WeightTotal { get; set; }

        [Column(TypeName = "decimal(8,2)")]
        public Nullable<decimal> Average { get; set; }
        public Nullable<int> Amount { get; set; }
        
        public Guid BillDetailId { get; set; }
        public BillDetail BillDetail { get; set; }

        public Guid GenerationId { get; set; }
    }
}