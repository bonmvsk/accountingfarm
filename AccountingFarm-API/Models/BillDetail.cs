using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountingFarm_API.Models
{
    public class BillDetail
    {
        private static readonly char delimiter = ';';
        private string _chicken;
        private string _basket;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string PrefixBill { get; set; }
        public DateTime CatchDate { get; set; }
        public int CatchAmount { get; set; }

        [NotMapped]
        public string[] WeightChickens
        {
            get { return _chicken.Split(delimiter); }
            set
            {
                _chicken = string.Join($"{delimiter}", value);
            }
        }

        [NotMapped]
        public string[] WeightBaskets
        {
            get { return _basket.Split(delimiter); }
            set
            {
                _basket = string.Join($"{delimiter}", value);
            }
        }

        public BillTotal BillTotal { get; set; }

        public Guid GenerationId { get; set; }
        public Guid CatcherId { get; set; }
        public Catcher Catcher { get; set; }

    }
}