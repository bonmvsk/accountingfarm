import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Generation } from '../_models/generation';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GenerationService {
  baseUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getAll(): Observable<Generation[]> {
    return this.http.get<Generation[]>(this.baseUrl + 'generation');
  }

  saveGeneration(generation: Generation) {
    return this.http.post(this.baseUrl + 'generation', generation);
  }

  updateGeneration(id: string, generation: Generation) {
    return this.http.put(this.baseUrl + 'generation/' + id, generation);
  }

  deleteGeneration(id: string) {
    return this.http.delete(this.baseUrl + 'generation/' + id);
  }
}
