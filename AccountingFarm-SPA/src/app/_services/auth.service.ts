import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../_models/user';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.apiUrl;
  jwtHelper = new JwtHelperService();
  decodedToken: any;
  photoUrl = new BehaviorSubject<string>('../../assets/user.png');
  currentPhotoUrl = this.photoUrl.asObservable();
  user = new BehaviorSubject<User>(null);
  currentUser = this.user.asObservable();

  constructor(private http: HttpClient) {}

  setCurrentUser(user: User) {
    this.user.next(user);
  }

  changeMemberPhoto(photoUrl: string) {
    this.photoUrl.next(photoUrl);
  }

  login(username: string, password: string) {
    return this.http
      .post(this.baseUrl + 'auth/login', { username: username, password: password })
      .pipe(
        map((response: any) => {
          const user = response;
          if (user) {
            localStorage.setItem('access_token', user.token);
            localStorage.setItem('user', JSON.stringify(user.user));
            this.decodedToken = this.jwtHelper.decodeToken(user.token);
            this.setCurrentUser(user.user);
          }
        })
      );
  }

  register(user: User) {
    return this.http.post(this.baseUrl + 'auth/register', user);
  }

  loggedIn() {
    const token = localStorage.getItem('access_token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  logout() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('user');
  }
}
