import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BillService {
  constructor(private http: HttpClient) {}

  getBillPrefix(generationId: string) {
    return this.http
      .post(environment.apiUrl + 'billdetail/prefix', { GenerationId: generationId })
      .pipe(
        map((p: string) => {
          return p;
        })
      );
  }
}
