import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, LOCALE_ID } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { JwtModule } from '@auth0/angular-jwt';
import localeTh from '@angular/common/locales/th';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarModule } from './nav-bar/nav-bar.module';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ErrorInterceptorProvider } from './_services/error.interceptor';
import { AuthService } from './_services/auth.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './_guards/auth.guard';
import { MatModule } from './mat.module';
import { SharesModule } from './_shares/shares.module';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { ItemsState } from './_state/items.state';
import { environment } from 'src/environments/environment.prod';
import { registerLocaleData } from '@angular/common';

export function tokenGetter() {
   return localStorage.getItem('access_token');
 }

 registerLocaleData(localeTh);

@NgModule({
   declarations: [
      AppComponent,
      DashboardComponent,
      RegisterComponent,
      LoginComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      FormsModule,
      JwtModule.forRoot({
         config: {
           tokenGetter: tokenGetter,
           whitelistedDomains: ['localhost:5000'],
           blacklistedRoutes: ['localhost:5000/api/auth']
         }
       }),
      ReactiveFormsModule,
      HttpClientModule,
      NgxsModule.forRoot([
         ItemsState
      ]),
      NgxsLoggerPluginModule.forRoot({ disabled: !environment.production }),
      NgxsReduxDevtoolsPluginModule.forRoot({ disabled: !environment.production }),
      NavBarModule,
      MatModule,
      SharesModule
   ],
   providers: [
      ErrorInterceptorProvider,
      { provide: MAT_DATE_LOCALE, useValue: 'th-TH' },
      { provide: LOCALE_ID, useValue: 'th'},
      AuthService,
      AuthGuard
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule {}
