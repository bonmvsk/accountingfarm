import { Item } from 'src/app/_models/item';

export class FetchItems {
  static readonly type = '[Items] Fetch Item';
}

export class FetchItemsCost {
  static readonly type = '[Items] Fetch Item Cost';
}

export class AddItemsAction {
  static readonly type = '[Items] Add item';
  constructor(public payload: Item) {}
}

export class UpdateItemsAction {
  static readonly type = '[Items] Update item';
  constructor(public id: string, public payload: Item) {}
}

export class RemoveItemsAction {
  static readonly type = '[Items] Remove item';
  constructor(public payload: string) {}
}
