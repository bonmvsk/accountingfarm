import { Generation } from '../_models/generation';

export class FetchGenerationReport {
  static readonly type = '[Generation] Fetch Report';
  constructor(public generationId: string) {}
}

export class FetchGenerations {
  static readonly type = '[Generation] Fetch Generations';
}

export class AddGenerationAction {
  static readonly type = '[Generation] Add Generation';
  constructor(public payload: Generation) {}
}

export class UpdateGenerationAction {
  static readonly type = '[Generation] Update Generation';
  constructor(public id: string, public payload: Generation) {}
}

export class RemoveGenerationAction {
  static readonly type = '[Generation] Remove Generation';
  constructor(public payload: string) {}
}

export class CloseGenerationAction {
  static readonly type = '[Generation] Close Generation';
  constructor(public id: string) {}
}
