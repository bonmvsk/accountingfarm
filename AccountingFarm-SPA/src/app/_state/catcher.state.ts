import { HttpClient } from '@angular/common/http';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import {
  AddCatcherAction,
  UpdateCatcherAction,
  RemoveCatcherAction,
  FetchCatcher
} from './catcher.actions';
import { environment } from 'src/environments/environment';
import { Catcher } from '../_models/catcher';
import { AlertifyService } from '../_services/alertify.service';
import { tap } from 'rxjs/operators';

export class CatcherStateModel {
  public catchers: Catcher[];
}

@State<CatcherStateModel>({
  name: 'catcher',
  defaults: {
    catchers: []
  }
})
export class CatcherState {
  @Selector()
  static getCatchers(state: CatcherStateModel) {
    return state.catchers;
  }

  constructor(private http: HttpClient, private alertify: AlertifyService) {}

  @Action(FetchCatcher)
  fetchCatcher({ getState, setState }: StateContext<CatcherStateModel>) {
    this.http.get(environment.apiUrl + 'catchers').subscribe(
      (catcher: Catcher[]) => {
        const state = getState();
        setState({
          ...state,
          catchers: catcher
        });
      },
      error => this.alertify.error(error)
    );
  }

  @Action(AddCatcherAction)
  add(ctx: StateContext<CatcherStateModel>, action: AddCatcherAction) {
    delete action.payload.id;
    this.http.post(environment.apiUrl + 'catchers', action.payload).subscribe(
      (res: Catcher) => {
        const state = ctx.getState();
        ctx.setState({ catchers: [...state.catchers, res] });
      },
      error => {
        this.alertify.error(error);
      }
    );
  }

  @Action(UpdateCatcherAction)
  update(ctx: StateContext<CatcherStateModel>, action: UpdateCatcherAction) {
    return this.http
      .put(environment.apiUrl + 'catchers/' + action.id, action.payload)
      .pipe(
        tap(() => {
          const state = ctx.getState();
          state.catchers.forEach((p, i) => {
            if (p.id === action.payload.id) {
              state.catchers[i] = action.payload;
              ctx.setState(state);
            }
          });
        })
      );
  }

  @Action(RemoveCatcherAction)
  remove(ctx: StateContext<CatcherStateModel>, action: RemoveCatcherAction) {
    return this.http.delete(environment.apiUrl + 'catchers/' + action.payload).subscribe(
      () => {},
      error => {
        this.alertify.error(error);
      },
      () => {
        const state = ctx.getState();
        ctx.setState({ catchers: state.catchers.filter(a => a.id !== action.payload) });
      }
    );
  }
}
