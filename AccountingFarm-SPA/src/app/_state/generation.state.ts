import { State, Action, StateContext, Selector, createSelector } from '@ngxs/store';
import {
  AddGenerationAction,
  RemoveGenerationAction,
  UpdateGenerationAction,
  FetchGenerations,
  FetchGenerationReport,
  CloseGenerationAction
} from './generation.actions';
import { Generation } from '../_models/generation';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AlertifyService } from '../_services/alertify.service';
import { GenerationReport } from '../_models/generation-report';

export class GenerationStateModel {
  public generation: Generation[];
  public report: GenerationReport;
}

@State<GenerationStateModel>({
  name: 'generation',
  defaults: {
    generation: [],
    report: null
  }
})
export class GenerationState {
  @Selector()
  static getAll(state: GenerationStateModel) {
    return state.generation;
  }

  @Selector()
  static getReport(state: GenerationStateModel) {
    return state.report;
  }

  constructor(private http: HttpClient, private alertify: AlertifyService) {}

  @Action(FetchGenerations)
  fetchGenerations(ctx: StateContext<GenerationStateModel>) {
    this.http.get(environment.apiUrl + 'generation/').subscribe(
      (generation: Generation[]) => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          generation: generation
        });
      },
      error => {
        this.alertify.error(error);
      }
    );
  }

  @Action(AddGenerationAction)
  add(ctx: StateContext<GenerationStateModel>, action: AddGenerationAction) {
    delete action.payload.id;
    delete action.payload.dateFeed;
    return this.http.post(environment.apiUrl + 'generation', action.payload).pipe(
      tap((generation: Generation) => {
        const state = ctx.getState();
        ctx.setState({ ...state, generation: [...state.generation, generation] });
      })
    );
  }

  @Action(UpdateGenerationAction)
  update(ctx: StateContext<GenerationStateModel>, action: UpdateGenerationAction) {
    delete action.payload.dateFeed;
    if (action.payload.dateIn) {
      action.payload.dateIn = new Date(action.payload.dateIn);
      action.payload.dateIn.setUTCHours(action.payload.dateIn.getUTCHours() + 7);
    }

    if (action.payload.dateOut) {
      action.payload.dateOut = new Date(action.payload.dateOut);
      action.payload.dateOut.setUTCHours(action.payload.dateOut.getUTCHours() + 7);
    }
    return this.http
      .put(environment.apiUrl + 'generation/' + action.id, action.payload)
      .pipe(
        tap((generation: Generation) => {
          const state = ctx.getState();
          state.generation.forEach((p, i) => {
            if (p.id === action.payload.id) {
              state.generation[i] = generation;
            }
          });
        })
      );
  }

  @Action(RemoveGenerationAction)
  remove(ctx: StateContext<GenerationStateModel>, action: RemoveGenerationAction) {
    return this.http.delete(environment.apiUrl + 'generation/' + action.payload).pipe(
      tap(() => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          generation: state.generation.filter(a => a.id !== action.payload)
        });
      })
    );
  }

  @Action(FetchGenerationReport)
  getReport(ctx: StateContext<GenerationStateModel>, action: FetchGenerationReport) {
    return this.http
      .get(environment.apiUrl + 'generation/' + action.generationId + '/report')
      .subscribe(
        (res: GenerationReport) => {
          const state = ctx.getState();
          ctx.setState({ ...state, report: res });
        }
      );
  }

  @Action(CloseGenerationAction)
  close(ctx: StateContext<GenerationStateModel>, action: CloseGenerationAction) {
    return this.http
      .put(`${environment.apiUrl}generation/${action.id}/close`, action.id)
      .pipe(
        tap((generation: Generation) => {
          const state = ctx.getState();
          state.generation.forEach((p, i) => {
            if (p.id === action.id) {
              state.generation[i] = generation;
            }
          });
        })
      );
  }
}
