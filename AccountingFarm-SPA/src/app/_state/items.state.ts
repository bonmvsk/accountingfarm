import { State, Action, StateContext, Selector } from '@ngxs/store';
import { AddItemsAction, RemoveItemsAction, UpdateItemsAction, FetchItems, FetchItemsCost } from './items.actions';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Item } from '../_models/item';
import { AlertifyService } from '../_services/alertify.service';
import { tap } from 'rxjs/internal/operators/tap';

export class ItemsStateModel {
  public item: Item[];
}

@State<ItemsStateModel>({
  name: 'item',
  defaults: {
    item: []
  }
})
export class ItemsState {
  @Selector()
  static getAll(state: ItemsStateModel) {
    return state.item;
  }

  constructor(private http: HttpClient, private alertify: AlertifyService) {}

  @Action(FetchItems)
  fetchitems({ getState, setState }: StateContext<ItemsStateModel>) {
    this.http.get(environment.apiUrl + 'item').subscribe(
      (item: Item[]) => {
        const state = getState();
        setState({
          ...state,
          item: item
        });
      },
      error => this.alertify.error(error)
    );
  }
  
  @Action(FetchItemsCost)
  fetchitemsCost({ getState, setState }: StateContext<ItemsStateModel>) {
    this.http.get(environment.apiUrl + 'item').subscribe(
      (item: Item[]) => {
        const state = getState();
        setState({
          ...state,
          item: item.filter(p => p.type !== 1)
        });
      },
      error => this.alertify.error(error)
    );
  }

  @Action(AddItemsAction)
  add(ctx: StateContext<ItemsStateModel>, action: AddItemsAction) {
    delete action.payload.id;
    return this.http.post(environment.apiUrl + 'item', action.payload).pipe(
      tap((item: Item) => {
        const state = ctx.getState();
        ctx.setState({ item: [...state.item, item] });
      })
    );
  }

  @Action(UpdateItemsAction)
  update(ctx: StateContext<ItemsStateModel>, action: UpdateItemsAction) {
    return this.http
      .put(environment.apiUrl + 'item/' + action.id, action.payload)
      .pipe(
        tap(() => {
          const state = ctx.getState();
          state.item.forEach((p, i) => {
            if (p.id === action.payload.id) {
              state.item[i] = action.payload;
              ctx.setState(state);
            }
          });
        })
      );
  }

  @Action(RemoveItemsAction)
  remove(ctx: StateContext<ItemsStateModel>, action: RemoveItemsAction) {
    return this.http.delete(environment.apiUrl + 'item/' + action.payload).subscribe(
      () => {},
      error => {
        this.alertify.error(error);
      },
      () => {
        const state = ctx.getState();
        ctx.setState({ item: state.item.filter(a => a.id !== action.payload) });
      }
    );
  }
}
