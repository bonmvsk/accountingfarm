import { Bill } from 'src/app/_models/bill';

export class AddBillAction {
  static readonly type = '[Bill] Add item';
  constructor(public payload: Bill) { }
}

export class FetchBill {
  static readonly type = '[Bill] Fetch item';
  constructor(public billId: string) { }
}

export class UpdateBillAction {
  static readonly type = '[Bill] Update item';
  constructor(public payload: Bill) { }
}

export class ResetBillAction {
  static readonly type = '[Bill] Reset item';
}

