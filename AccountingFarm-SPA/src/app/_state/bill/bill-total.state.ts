import { State, Action, StateContext, Selector } from '@ngxs/store';
import { FetchBillTotal, RemoveBillTotalAction } from './bill-total.actions';
import { HttpClient } from '@angular/common/http';
import { BillTotal } from 'src/app/_models/billTotal';
import { environment } from 'src/environments/environment';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { map } from 'rxjs/operators';

export class BillTotalStateModel {
  public items: BillTotal[];
}

@State<BillTotalStateModel>({
  name: 'billTotal',
  defaults: {
    items: []
  }
})
export class BillTotalState {
  @Selector()
  static getBillTotalAll(state: BillTotalStateModel) {
    return state.items;
  }

  constructor(private http: HttpClient, private alertify: AlertifyService) {}

  @Action(FetchBillTotal)
  fetchBillTotal(ctx: StateContext<BillTotalStateModel>, action: FetchBillTotal) {
    return this.http
      .get(environment.apiUrl + 'billdetail/' + action.generationId + '/total')
      .subscribe((res: BillTotal[]) => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          items: res
        });
      });
  }

  @Action(RemoveBillTotalAction)
  remove(ctx: StateContext<BillTotalStateModel>, action: RemoveBillTotalAction) {
    return this.http.delete(environment.apiUrl + 'billDetail/' + action.payload).pipe(
      map(
        response => {
          if (response) {
            const state = ctx.getState();
            ctx.setState({
              items: state.items.filter(a => a.billDetailId !== action.payload)
            });
          }
          return response;
        },
        error => {
          return error;
        }
      )
    );
  }
}
