import { State, Action, StateContext, Selector } from '@ngxs/store';
import {
  AddBillAction,
  FetchBill,
  UpdateBillAction,
  ResetBillAction
} from './bill.actions';
import { Bill } from 'src/app/_models/bill';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';
import { AlertifyService } from 'src/app/_services/alertify.service';

export class BillStateModel {
  public bill: Bill;
}

@State<BillStateModel>({
  name: 'bill',
  defaults: {
    bill: null
  }
})
export class BillState {
  @Selector()
  static getBillAll(state: BillStateModel) {
    return state.bill;
  }

  constructor(private http: HttpClient, private alertify: AlertifyService) {}

  @Action(FetchBill)
  fetchBill({ getState, setState }: StateContext<BillStateModel>, action: FetchBill) {
    return this.http.get(environment.apiUrl + 'billDetail/' + action.billId).subscribe(
      (item: Bill) => {
        const state = getState();
        setState({
          ...state,
          bill: item
        });
      },
      error => this.alertify.error(error)
    );
  }

  @Action(AddBillAction)
  add(ctx: StateContext<BillStateModel>, action: AddBillAction) {
    return this.http.post(environment.apiUrl + 'billdetail', action.payload).pipe(
      map(
        (res: Bill) => {
          const state = ctx.getState();
          ctx.setState({ bill: action.payload });
          return res;
        },
        catchError(err => {
          return err;
        })
      )
    );
  }

  @Action(UpdateBillAction)
  update(ctx: StateContext<BillStateModel>, action: UpdateBillAction) {
    this.http
      .put(environment.apiUrl + 'billdetail/' + action.payload.id, action.payload)
      .subscribe(
        (res: Bill) => {
          ctx.setState({ bill: null });
        },
        catchError(err => {
          return err;
        })
      );
  }

  @Action(ResetBillAction)
  resetWeight(ctx: StateContext<BillStateModel>) {
    const state = ctx.getState();
    ctx.setState({ bill: null });
  }
}
