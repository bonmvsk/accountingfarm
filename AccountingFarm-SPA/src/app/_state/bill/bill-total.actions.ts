export class BillTotalAction {
  static readonly type = '[BillTotal] Add item';
  constructor(public payload: string) { }
}

export class FetchBillTotal {
  static readonly type = '[BillTotal] Fetch item';
  constructor(public generationId: string) { }
}

export class RemoveBillTotalAction {
  static readonly type = '[BillTotal] Remove item';
  constructor(public payload: string) { }
}

