export class AddWeightChickenAction {
  static readonly type = '[WeightChicken] Add item';
  constructor(public payload: string) { }
}

export class InitWeightChickenAction {
  static readonly type = '[WeightChicken] Init item';
  constructor(public payload: string[]) { }
}

export class EditWeightChickenAction {
  static readonly type = '[WeightChicken] Edit item';
  constructor(public index: number, public value: string) { }
}

export class RemoveWeightChickenAction {
  static readonly type = '[WeightChicken] Remove item';
  constructor(public payload: number) { }
}

export class InitWeightBasketAction {
  static readonly type = '[WeightBasket] Init item';
  constructor(public payload: string[]) { }
}

export class AddWeightBasketAction {
  static readonly type = '[WeightBasket] Add item';
  constructor(public payload: string) { }
}

export class EditWeightBasketAction {
  static readonly type = '[WeightBasket] Edit item';
  constructor(public index: number, public value: string) { }
}

export class RemoveWeightBasketAction {
  static readonly type = '[WeightBasket] Remove item';
  constructor(public payload: number) { }
}

export class ResetWeightAction {
  static readonly type = '[WeightBasket] Reset item';
}
