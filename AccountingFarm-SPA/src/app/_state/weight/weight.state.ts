import { State, Action, StateContext, Selector } from '@ngxs/store';
import {
  AddWeightChickenAction,
  AddWeightBasketAction,
  ResetWeightAction,
  EditWeightChickenAction,
  EditWeightBasketAction,
  RemoveWeightChickenAction,
  RemoveWeightBasketAction,
  InitWeightChickenAction,
  InitWeightBasketAction
} from './weight.actions';

export class WeightStateModel {
  public chicken: string[];
  public basket: string[];
}

@State<WeightStateModel>({
  name: 'weight',
  defaults: {
    chicken: [],
    basket: []
  }
})
export class WeightState {
  @Selector()
  static getWeightChicken(state: WeightStateModel) {
    return state.chicken;
  }

  @Selector()
  static getWeightBasket(state: WeightStateModel) {
    return state.basket;
  }

  @Action(InitWeightChickenAction)
  initWeightChicken(
    ctx: StateContext<WeightStateModel>,
    action: InitWeightChickenAction
  ) {
    const state = ctx.getState();
    ctx.setState({ ...state, chicken: action.payload });
  }

  @Action(AddWeightChickenAction)
  addWeightChicken(ctx: StateContext<WeightStateModel>, action: AddWeightChickenAction) {
    const state = ctx.getState();
    ctx.setState({ ...state, chicken: [...state.chicken, action.payload] });
  }

  @Action(EditWeightChickenAction)
  updateChicken(ctx: StateContext<WeightStateModel>, action: EditWeightChickenAction) {
    const state = ctx.getState();
    state.chicken[action.index] = action.value;
    ctx.setState(state);
  }

  @Action(RemoveWeightChickenAction)
  removeChicken(ctx: StateContext<WeightStateModel>, action: RemoveWeightChickenAction) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      chicken: state.chicken.filter((a, i) => i !== action.payload)
    });
  }

  @Action(InitWeightBasketAction)
  initWeightBasket(ctx: StateContext<WeightStateModel>, action: InitWeightBasketAction) {
    const state = ctx.getState();
    ctx.setState({ ...state, basket: action.payload });
  }

  @Action(AddWeightBasketAction)
  addWeightBasket(ctx: StateContext<WeightStateModel>, action: AddWeightBasketAction) {
    const state = ctx.getState();
    ctx.setState({ ...state, basket: [...state.basket, action.payload] });
  }

  @Action(EditWeightBasketAction)
  updateBasket(ctx: StateContext<WeightStateModel>, action: EditWeightBasketAction) {
    const state = ctx.getState();
    state.basket[action.index] = action.value;
    ctx.setState(state);
  }

  @Action(RemoveWeightBasketAction)
  removeBasket(ctx: StateContext<WeightStateModel>, action: RemoveWeightBasketAction) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      basket: state.basket.filter((a, i) => i !== action.payload)
    });
  }

  @Action(ResetWeightAction)
  resetWeight(ctx: StateContext<WeightStateModel>, action: ResetWeightAction) {
    const state = ctx.getState();
    ctx.setState({ ...state, chicken: [], basket: [] });
  }
}
