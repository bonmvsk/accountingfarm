import { Catcher } from '../_models/catcher';

export class FetchCatcher {
  static readonly type = '[Catcher] Fetch Catchers';
}

export class AddCatcherAction {
  static readonly type = '[Catcher] Add Catcher';
  constructor(public payload: Catcher) { }
}

export class UpdateCatcherAction {
  static readonly type = '[Catcher] Update Catcher';
  constructor(public id: string, public payload: Catcher) { }
}

export class RemoveCatcherAction {
  static readonly type = '[Catcher] Remove Catcher';
  constructor(public payload: string) { }
}

