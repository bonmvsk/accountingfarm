import { State, Action, StateContext, Selector } from '@ngxs/store';
import {
  FetchCost,
  AddCostAction,
  UpdateCostAction,
  RemoveCostAction
} from './cost.actions';
import { Cost } from 'src/app/_models/cost';
import { HttpClient } from '@angular/common/http';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { environment } from 'src/environments/environment';
import { tap, map, catchError } from 'rxjs/operators';

export class CostStateModel {
  public costs: Cost[];
}

@State<CostStateModel>({
  name: 'cost',
  defaults: {
    costs: []
  }
})
export class CostState {
  @Selector()
  static getCosts(state: CostStateModel) {
    return state.costs;
  }

  constructor(private http: HttpClient, private alertify: AlertifyService) {}

  @Action(FetchCost)
  fetchCost(ctx: StateContext<CostStateModel>, action: FetchCost) {
    this.http
      .get(environment.apiUrl + 'cost/' + action.generationId)
      .subscribe((res: Cost[]) => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          costs: res
        });
      });
  }

  @Action(AddCostAction)
  add(ctx: StateContext<CostStateModel>, action: AddCostAction) {
    delete action.payload.id;
    return this.http.post(environment.apiUrl + 'cost', action.payload).pipe(
      map((res: Cost) => {
        const state = ctx.getState();
        ctx.setState({ costs: [...state.costs, res] });

        return res;
      }),
      catchError(error => {
        return error;
      })
    );
  }

  @Action(UpdateCostAction)
  update(ctx: StateContext<CostStateModel>, action: UpdateCostAction) {
    return this.http.put(environment.apiUrl + 'cost/' + action.id, action.payload).pipe(
      tap(
        (res: Cost) => {
          const state = ctx.getState();
          state.costs.forEach((p, i) => {
            if (p.id === action.payload.id) {
              state.costs[i] = res;
              ctx.setState(state);
            }
          });
        },
        error => {}
      )
    );
  }

  @Action(RemoveCostAction)
  remove(ctx: StateContext<CostStateModel>, action: RemoveCostAction) {
    return this.http.delete(environment.apiUrl + 'cost/' + action.payload).pipe(
      map(res => {
        if (res) {
          const state = ctx.getState();
          ctx.setState({ costs: state.costs.filter(a => a.id !== action.payload) });
        }
        return res;
      }),
      catchError(error => {
        return error;
      })
    );
  }
}
