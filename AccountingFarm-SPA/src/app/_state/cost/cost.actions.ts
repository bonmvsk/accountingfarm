import { Cost } from 'src/app/_models/cost';

export class FetchCost {
  static readonly type = '[Cost] Fetch item';
  constructor(public generationId: string) { }
}

export class AddCostAction {
  static readonly type = '[Cost] Add item';
  constructor(public payload: Cost) { }
}

export class UpdateCostAction {
  static readonly type = '[Cost] Update item';
  constructor(public id: string, public payload: Cost) { }
}

export class RemoveCostAction {
  static readonly type = '[Cost] Remove item';
  constructor(public payload: string) { }
}
