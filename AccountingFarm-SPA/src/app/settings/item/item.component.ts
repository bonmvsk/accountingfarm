import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Store, Select } from '@ngxs/store';
import {
  AddItemsAction,
  RemoveItemsAction,
  UpdateItemsAction,
  FetchItems
} from 'src/app/_state/items.actions';
import { ItemsState } from 'src/app/_state/items.state';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap, delay } from 'rxjs/operators';
import { Item } from 'src/app/_models/item';

@Component({
  selector: 'af-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit, AfterViewInit {
  @Select(ItemsState.getAll) item$: Observable<Item[]>;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  displayedColumns: string[] = ['index', 'name', 'rates', 'action'];
  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = false;

  itemForm: FormGroup;
  data = new MatTableDataSource<Item>();

  constructor(
    private fb: FormBuilder,
    private alertify: AlertifyService,
    private store: Store
  ) {}

  ngOnInit() {
    this.store.dispatch(new FetchItems());
    this.createForm();
    this.data.paginator = this.paginator;
  }

  createForm() {
    this.itemForm = this.fb.group({
      id: [''],
      type: [0, Validators.required],
      name: ['', Validators.required],
      rates: ['', Validators.compose([Validators.required])]
    });
  }

  onSave(item: Item) {
    if (this.itemForm.valid) {
      if (item.id) {
        this.store.dispatch(new UpdateItemsAction(item.id, this.itemForm.value)).subscribe(
          () => {},
          error => this.alertify.error(error),
          () => {
            this.alertify.success('แก้ไขสำเร็จ');
            this.itemForm.reset();
            this.ngAfterViewInit();
          }
        );
      } else {
        this.store.dispatch(new AddItemsAction(this.itemForm.value)).subscribe(
          () => {},
          error => this.alertify.error(error),
          () => {
            this.alertify.success('บันทึกสำเร็จ');
            this.ngAfterViewInit();
            this.itemForm.reset();
          }
        );
      }
    }
  }

  onDelete(item: Item) {
    if (item) {
      this.alertify
        .confirm('คุณต้องการลบ ' + item.name + ' ใช่หรือไม่?')
        .afterClosed()
        .subscribe(res => {
          if (res) {
            this.store.dispatch(new RemoveItemsAction(item.id)).subscribe(
              () => {},
              error => {
                this.alertify.error(error);
              },
              () => {
                this.itemForm.reset();
                this.alertify.success('ลบสำเร็จ');
              }
            );
          }
        });
    }
  }

  onSelected(item: Item) {
    this.itemForm.setValue(item);
  }

  onCancle() {
    this.itemForm.reset();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        delay(0),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.item$;
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.length;
          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      )
      .subscribe((data: Item[]) => {
        this.data.data = data;
      });
  }
}
