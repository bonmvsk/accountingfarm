import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';

import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap, delay } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Catcher } from 'src/app/_models/catcher';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { CatcherState } from 'src/app/_state/catcher.state';
import {
  UpdateCatcherAction,
  AddCatcherAction,
  RemoveCatcherAction,
  FetchCatcher
} from 'src/app/_state/catcher.actions';
import { Provinces } from 'src/app/_models/provinces';
import provinces from 'src/assets/_data/provinces.json';

@Component({
  selector: 'af-catcher',
  templateUrl: './catcher.component.html',
  styleUrls: ['./catcher.component.scss']
})
export class CatcherComponent implements OnInit, AfterViewInit {
  @Select(CatcherState.getCatchers) catcher$: Observable<Catcher[]>;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  options: Provinces[] = [];
  filteredOptions: Observable<Provinces[]>;

  displayedColumns: string[] = [
    'index',
    'nickname',
    'fullname',
    'licensePlate',
    'action'
  ];
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  catcherForm: FormGroup;
  data = new MatTableDataSource<Catcher>();

  constructor(
    private fb: FormBuilder,
    private alertify: AlertifyService,
    private store: Store
  ) {}

  ngOnInit() {
    this.store.dispatch(new FetchCatcher());
    this.options = provinces;
    this.createForm();

    this.filteredOptions = this.catcherForm.get('licenseCountry').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );

    this.data.paginator = this.paginator;
  }

  private _filter(value: string): Provinces[] {
    if (value) {
      const filterValue = value.toLowerCase();
      return this.options.filter(option =>
        option.PROVINCE_NAME.toLowerCase().includes(filterValue)
      );
    }
    return null;
  }

  createForm() {
    this.catcherForm = this.fb.group({
      id: [''],
      nickname: ['', Validators.required],
      firstname: [''],
      lastname: [''],
      licensePlate: [''],
      licenseCountry: ['']
    });
  }

  onSave(item: Catcher) {
    if (this.catcherForm.valid) {
      if (item.id) {
        this.store
          .dispatch(new UpdateCatcherAction(item.id, this.catcherForm.value))
          .subscribe(
            () => {
              this.alertify.success('แก้ไขสำเร็จ');
              this.catcherForm.reset();
              this.ngAfterViewInit();
            },
            error => this.alertify.error(error),
            () => {}
          );
      } else {
        this.store.dispatch(new AddCatcherAction(this.catcherForm.value)).subscribe(
          () => {
            this.alertify.success('บันทึกสำเร็จ');
            this.ngAfterViewInit();
            this.catcherForm.reset();
          },
          error => this.alertify.error(error),
          () => {}
        );
      }
    }
  }

  onDelete(catcher: Catcher) {
    if (catcher) {
      this.alertify
        .confirm('คุณต้องการลบ ' + catcher.nickname + ' ใช่หรือไม่?')
        .afterClosed()
        .subscribe(res => {
          if (res) {
            this.store.dispatch(new RemoveCatcherAction(catcher.id)).subscribe(
              () => {},
              error => {
                this.alertify.error(error);
              },
              () => {
                this.catcherForm.reset();
                this.alertify.success('ลบสำเร็จ');
              }
            );
          }
        });
    }
  }

  onSelected(catcher: Catcher) {
    this.catcherForm.setValue(catcher);
  }

  onCancle() {
    this.catcherForm.reset();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        delay(0),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.catcher$;
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.length;
          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      )
      .subscribe((data: Catcher[]) => {
        this.data.data = data;
      });
  }
}
