import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from './settings.component';
import { ItemComponent } from './item/item.component';
import { CatcherComponent } from './catcher/catcher.component';

const routes: Routes = [
  { path: '', component: SettingsComponent },
  { path: 'item', component: ItemComponent },
  { path: 'catcher', component: CatcherComponent },
];

export const SettingsRoutes = RouterModule.forChild(routes);
