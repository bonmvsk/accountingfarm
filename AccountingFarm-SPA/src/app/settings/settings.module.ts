import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxsModule } from '@ngxs/store';

import { MatModule } from '../mat.module';
import { SettingsRoutes } from './settings.routing';
import { SettingsComponent } from './settings.component';
import { ItemComponent } from './item/item.component';
import { CatcherComponent } from './catcher/catcher.component';
import { ItemsState } from '../_state/items.state';
import { CatcherState } from '../_state/catcher.state';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SettingsRoutes,
    MatModule,
    NgxsModule.forFeature([
      ItemsState,
      CatcherState
   ]),
  ],
  declarations: [SettingsComponent, ItemComponent, CatcherComponent]
})
export class SettingsModule { }
