import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { AlertifyService } from '../_services/alertify.service';
import { MatModule } from '../mat.module';
import { FooterComponent } from './footer/footer.component';
import { ThaiDatePipe } from '../_diractive/thai-date.pipe';

@NgModule({
  imports: [
    CommonModule,
    MatModule
  ],
  declarations: [
    ConfirmDialogComponent,
    FooterComponent,
    ThaiDatePipe
  ],
  exports: [
    FooterComponent,
    ThaiDatePipe
  ],
  providers: [
    AlertifyService
  ],
  entryComponents: [ConfirmDialogComponent]
})
export class SharesModule { }
