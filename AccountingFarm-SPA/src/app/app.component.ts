import { Component, OnInit } from '@angular/core';
import { AuthService } from './_services/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from './_models/user';
import { Store } from '@ngxs/store';
import { FetchItems } from './_state/items.actions';

@Component({
  selector: 'af-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  jwtHelper = new JwtHelperService();

  constructor(private authService: AuthService, private store: Store) {
  }

  ngOnInit() {
    const token = localStorage.getItem('access_token');
    const user: User = JSON.parse(localStorage.getItem('user'));
    if (token) {
      this.authService.decodedToken = this.jwtHelper.decodeToken(token);
    }

    if (user) {
      this.authService.setCurrentUser(user);
      this.authService.changeMemberPhoto(user.photoUrl);
    }

    this.store.dispatch(new FetchItems());
  }
}
