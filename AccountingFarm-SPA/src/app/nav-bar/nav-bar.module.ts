import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar.component';
import { MatModule } from '../mat.module';
import { RouterModule } from '@angular/router';
import { SharesModule } from '../_shares/shares.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatModule,
    SharesModule
  ],
  declarations: [
    NavBarComponent
  ],
  exports: [
    NavBarComponent
  ]
})
export class NavBarModule {}
