import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { BreakpointState, BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';
import { User } from '../_models/user';
import { Item } from '../_models/item';
import { Select, Store } from '@ngxs/store';
import { ItemsState } from '../_state/items.state';

@Component({
  selector: 'af-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  @Select(ItemsState.getAll) item$: Observable<Item[]>;
  hasItem: boolean;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(
    Breakpoints.Handset
  );
  userLogin: User;
  photoUrl: string;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authService.currentUser.subscribe(user => (this.userLogin = user));
    this.authService.currentPhotoUrl.subscribe(photoUrl => (this.photoUrl = photoUrl));

    this.item$.subscribe(res => {
      if (res.length === 0) {
        this.hasItem = false;
      } else {
        this.hasItem = true;
      }
    });
  }

  loggedIn() {
    return this.authService.loggedIn();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
