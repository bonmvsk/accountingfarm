import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { UserRoutingRoutes } from './user-routing.routing';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingRoutes
  ],
  declarations: [UserComponent]
})
export class UsersModule { }
