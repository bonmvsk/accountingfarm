import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';
import { MatTabChangeEvent } from '@angular/material/tabs';

@Component({
  selector: 'af-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  // currentTab: number;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private alertify: AlertifyService,
    private router: Router
  ) {}

  ngOnInit() {
    this.createLoginForm();
  }

  createLoginForm() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    if (this.loginForm.valid) {
      this.authService
        .login(this.loginForm.get('username').value, this.loginForm.get('password').value)
        .subscribe(
          next => {
            this.alertify.success('Logged in successfully');
          },
          error => {
            this.alertify.error(error);
          },
          () => {
            this.router.navigate(['/first']);
          }
        );
    }
  }

  cancelLogin() {
    this.loginForm.reset();
  }

  onTabClick(event: MatTabChangeEvent) {
    // this.currentTab = event.index;
    // console.log('event => ', event);
    // console.log('index => ', event.index);
    // console.log('tab => ', event.tab);
  }
}
