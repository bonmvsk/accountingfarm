export class Catcher {
  id: string;
  nickname: string;
  firstname: string;
  lastname: string;
  licensePlate: string;
  licenseCountry: string;
}
