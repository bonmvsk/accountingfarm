import { Photo } from './photo';

export interface User {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  gender: string;
  dateOfBirth: Date;
  photoUrl: string;
  photos?: Photo[];
}
