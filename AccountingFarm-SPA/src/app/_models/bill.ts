export interface Bill {
  id: string;
  prefixBill: string;
  catchDate: Date;
  catchAmount: number;
  weightChickens: string[];
  weightBaskets: string[];
  generationId: string;
  catcherId: string;
}
