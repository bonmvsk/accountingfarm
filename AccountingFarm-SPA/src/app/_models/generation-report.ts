export interface GenerationReport {
  priceInsurance: number;
  weightTotal: number;
  catchAmount: number;
  average: number;
  chickenLeft: number;
  priceChicken: number;
  priceFood: number;
  priceMedicine: number;
  priceOther: number;
  totalCost: number;
  totalSold: number;
  profit: number;
  dateOut: Date;
}
