export interface Generation {
    id: string;
    name: string;
    dateIn: Date;
    dateOut: Date;
    dateFeed: string;
}
