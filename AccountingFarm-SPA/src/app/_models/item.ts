export interface Item {
    id: string;
    type: number;
    name: string;
    rates: number;
}
