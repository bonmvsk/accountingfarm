import { Item } from './item';

export class Cost {
  id: string;
  amount: number;
  price: number;
  itemId: string;
  item: Item;
  generationId: string;
}
