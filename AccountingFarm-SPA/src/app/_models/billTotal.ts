export interface BillTotal {
  id: string;
  prefix: string;
  weightChicken: number;
  weightBasket: number;
  weightTotal: number;
  average: number;
  amount: number;
  billDetailId: string;
}
