export class BreadCrumb {
    label: string;
    url: string;
    params?: string;
}
