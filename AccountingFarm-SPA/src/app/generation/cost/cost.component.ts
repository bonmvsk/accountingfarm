import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, merge, of as observableOf } from 'rxjs';
import { startWith, delay, switchMap, map, catchError } from 'rxjs/operators';
import { Select, Store } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { ItemsState } from 'src/app/_state/items.state';
import { Item } from 'src/app/_models/item';
import { FetchItemsCost } from 'src/app/_state/items.actions';
import { AlertifyService } from 'src/app/_services/alertify.service';
import {
  AddCostAction,
  UpdateCostAction,
  FetchCost,
  RemoveCostAction
} from 'src/app/_state/cost/cost.actions';
import { Cost } from 'src/app/_models/cost';
import { CostState } from 'src/app/_state/cost/cost.state';
import { FetchGenerationReport } from 'src/app/_state/generation.actions';

@Component({
  selector: 'af-cost',
  templateUrl: './cost.component.html',
  styleUrls: ['./cost.component.scss']
})
export class CostComponent implements OnInit, AfterViewInit {
  @Select(CostState.getCosts) cost$: Observable<Cost[]>;
  @Select(ItemsState.getAll) item$: Observable<Item[]>;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  displayedColumns: string[] = ['index', 'name', 'amount', 'price', 'action'];
  displayedFooterColumns: string[] = ['index', 'name', 'amount', 'price', 'action'];
  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = false;

  generationId: string;
  addItemForm: FormGroup;
  data = new MatTableDataSource<Cost>();

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private alertify: AlertifyService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.generationId = this.route.snapshot.params.id;
    this.store.dispatch(new FetchCost(this.generationId));
    this.store.dispatch(new FetchItemsCost());
    this.createForm();
    this.data.paginator = this.paginator;
  }

  createForm() {
    this.addItemForm = this.fb.group({
      id: [''],
      itemId: ['', Validators.required],
      amount: ['', Validators.required],
      generationId: ['']
    });
  }

  onSave(cost: Cost) {
    cost.generationId = this.generationId;
    if (this.addItemForm.valid) {
      if (cost.id) {
        this.store
          .dispatch(new UpdateCostAction(cost.id, this.addItemForm.value))
          .subscribe(
            () => {},
            error => this.alertify.error(error),
            () => {
              this.alertify.success('แก้ไขสำเร็จ');
              this.store.dispatch(new FetchGenerationReport(this.generationId));
              this.onCancle();
              this.ngAfterViewInit();
            }
          );
      } else {
        this.store.dispatch(new AddCostAction(cost)).subscribe(
          res => {
            if (res) {
              this.alertify.success('บันทึกสำเร็จ');
              this.store.dispatch(new FetchGenerationReport(this.generationId));
              this.onCancle();
            }
          },
          error => this.alertify.error(error)
        );
      }
    }
  }

  onCancle() {
    this.addItemForm.reset();
  }

  onSelected(cost: Cost) {
    this.addItemForm.setValue({
      id: cost.id,
      itemId: cost.item.id,
      amount: cost.amount,
      generationId: cost.generationId
    });
  }

  onDelete(cost: Cost) {
    this.alertify
      .confirm('คุณต้องการลบ ' + cost.item.name + 'ใช่หรือไม่?')
      .afterClosed()
      .subscribe(res => {
        if (res) {
          this.store.dispatch(new RemoveCostAction(cost.id)).subscribe(
            result => {
              if (result) {
                this.addItemForm.reset();
                this.alertify.success('ลบสำเร็จ');
                this.store.dispatch(new FetchGenerationReport(this.generationId));
              }
            },
            error => {
              this.alertify.error(error);
            }
          );
        }
      });
  }

  getTotalCost() {
    let total: any;
    this.cost$.subscribe(res => (total = res));
    return total.map((t: Cost) => t.price).reduce((acc, value) => acc + value, 0);
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        delay(0),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.cost$;
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.length;
          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      )
      .subscribe((data: Cost[]) => {
        this.data.data = data;
      });
  }
}
