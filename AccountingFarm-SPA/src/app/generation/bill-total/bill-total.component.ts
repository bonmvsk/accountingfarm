import { Component, OnInit, AfterViewInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Select, Store } from '@ngxs/store';
import { BillTotalState } from 'src/app/_state/bill/bill-total.state';
import { Observable, merge, of as observableOf } from 'rxjs';
import { startWith, delay, switchMap, map, catchError } from 'rxjs/operators';
import { BillTotal } from 'src/app/_models/billTotal';
import {
  FetchBillTotal,
  RemoveBillTotalAction
} from 'src/app/_state/bill/bill-total.actions';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { FetchGenerationReport } from 'src/app/_state/generation.actions';

@Component({
  selector: 'af-bill-total',
  templateUrl: './bill-total.component.html',
  styleUrls: ['./bill-total.component.scss']
})
export class BillTotalComponent implements OnInit, AfterViewInit {
  @Input() GenerationId: string;
  @Select(BillTotalState.getBillTotalAll) billTotal$: Observable<BillTotal[]>;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  displayedColumns: string[] = [
    'billNo',
    'chicken',
    'basket',
    'total',
    'average',
    'amount',
    'action'
  ];
  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = false;
  data = new MatTableDataSource<BillTotal>();

  generationId: string;

  constructor(
    private route: ActivatedRoute,
    private store: Store,
    private alertify: AlertifyService
  ) {}

  ngOnInit() {
    this.generationId = this.route.snapshot.params.id;
    this.store.dispatch(new FetchBillTotal(this.generationId));
    this.data.paginator = this.paginator;
  }

  onDelete(billTotal: BillTotal) {
    this.alertify
      .confirm('คุณต้องการลบบิลเลขที่ ' + billTotal.prefix + ' ใช่หรือไม่?')
      .afterClosed()
      .subscribe(res => {
        if (res) {
          this.store
            .dispatch(new RemoveBillTotalAction(billTotal.billDetailId))
            .subscribe(
              response => {
                if (response) {
                  this.alertify.success('ลบสำเร็จ');
                  this.ngAfterViewInit();
                  this.store.dispatch(new FetchGenerationReport(this.generationId));
                }
              },
              error => this.alertify.error(error)
            );
        }
      });
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        delay(0),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.billTotal$;
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.length;
          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      )
      .subscribe((data: BillTotal[]) => {
        this.data.data = data;
      });
  }
}
