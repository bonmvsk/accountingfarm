import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GenerationState } from 'src/app/_state/generation.state';
import { GenerationReport } from 'src/app/_models/generation-report';
import {
  FetchGenerationReport,
  CloseGenerationAction,
  FetchGenerations
} from 'src/app/_state/generation.actions';
import { Generation } from 'src/app/_models/generation';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Location } from '@angular/common';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'af-generation-detail',
  templateUrl: './generation-detail.component.html',
  styleUrls: ['./generation-detail.component.scss']
})
export class GenerationDetailComponent implements OnInit {
  @Select(GenerationState.getReport) report$: Observable<GenerationReport>;
  @Select(GenerationState.getAll) generations$: Observable<Generation>;

  report: GenerationReport;
  test: Observable<Generation>;
  test2: Generation;

  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  barChartType = 'bar';
  barChartLegend = true;

  barChartData: any[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];

  // events
  chartClicked(e: any): void {
    console.log(e);
  }

  chartHovered(e: any): void {
    console.log(e);
  }

  constructor(
    private route: ActivatedRoute,
    private localtion: Location,
    private store: Store,
    private alertify: AlertifyService
  ) {}

  ngOnInit() {
    this.store.dispatch(new FetchGenerationReport(this.route.snapshot.params.id));

    this.report$.subscribe((val: GenerationReport) => {
      this.report = val;
    });
  }

  onClickCloseGeneration() {
    this.store
      .dispatch(new CloseGenerationAction(this.route.snapshot.params.id))
      .subscribe(
        () => {
          this.alertify.success('ปิดรุ่นเรียบร้อย');
          this.localtion.back();
        },
        error => {
          this.alertify.error(error);
        },
        () => {}
      );
  }
}
