import { Inject, Component, OnInit, Optional } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Generation } from 'src/app/_models/generation';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'af-generation-dialog',
  templateUrl: './generation-dialog.html'
})

// tslint:disable-next-line:component-class-suffix
export class GenerationOverviewDialog implements OnInit {
  generationForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<GenerationOverviewDialog>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Generation,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.createForm();

    this.dialogRef.keydownEvents().subscribe(res => {
      if (this.generationForm.valid && res.key === 'Enter') {
        this.dialogRef.close(this.generationForm.value);
      }
    });
  }

  createForm() {
    this.generationForm = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      dateIn: [Date, Validators.required],
      dateOut: ['']
    });
    if (this.data !== null) {
      this.generationForm.patchValue(this.data);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  minDate (): Date {
    const date = new Date(this.generationForm.get('dateIn').value);
    date.setDate(date.getDate() + 1);
    return date;
  }
}
