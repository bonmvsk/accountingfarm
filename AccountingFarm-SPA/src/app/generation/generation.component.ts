import { Component, OnInit } from '@angular/core';
import { Generation } from '../_models/generation';
import { MatDialog } from '@angular/material/dialog';
import { GenerationOverviewDialog } from './generation-overview-dialog/generation.dialog';
import { AlertifyService } from '../_services/alertify.service';
import { Store, Select } from '@ngxs/store';
import { AddGenerationAction, RemoveGenerationAction, UpdateGenerationAction, FetchGenerations } from '../_state/generation.actions';
import { Observable } from 'rxjs';
import { GenerationState } from '../_state/generation.state';

@Component({
  selector: 'af-generation',
  templateUrl: './generation.component.html',
  styleUrls: ['./generation.component.scss']
})
export class GenerationComponent implements OnInit {
  @Select(GenerationState.getAll) generations$: Observable<Generation>;
  generations: Generation[];

  constructor(
    public dialog: MatDialog,
    private alertify: AlertifyService,
    private store: Store
  ) {}

  ngOnInit() {
    this.store.dispatch(new FetchGenerations());
  }

  openDialog(gen?: Generation) {
    const dialogRef = this.dialog.open(GenerationOverviewDialog, {
      width: '300px',
      data: gen
    });
    dialogRef.afterClosed().subscribe((result: Generation) => {
      if (result) {
        if (gen) {
          this.update(gen.id, result);
        } else {
          this.save(result);
        }
      }
    });
  }

  private save(result: Generation) {
    this.store.dispatch(new AddGenerationAction(result)).subscribe(
      () => {},
      error => {
        this.alertify.error(error);
      },
      () => {
        this.alertify.success('บันทึกสำเร็จ');
      }
    );
  }

  private update(id: string, result: Generation) {
    this.store.dispatch(new UpdateGenerationAction(id, result)).subscribe(
      () => {
        this.alertify.success('แก้ไขสำเร็จ');
      },
      error => {
        this.alertify.error(error);
      },
      () => {
      }
    );
  }

  onDeleteClick(gen: Generation) {
    this.alertify
      .confirm('คุณต้องการที่จะลบ "' + gen.name + '" ใช่หรือไม่?')
      .afterClosed()
      .subscribe(res => {
        if (res) {
          this.store.dispatch(new RemoveGenerationAction(gen.id)).subscribe(
            () => {},
            error => {
              this.alertify.error(error);
            },
            () => {
              this.alertify.success('ลบ ' + gen.name + ' เรียบร้อย');
            }
          );
        }
      });
  }
}
