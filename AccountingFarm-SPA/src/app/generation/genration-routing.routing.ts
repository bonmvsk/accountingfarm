import { Routes, RouterModule } from '@angular/router';
import { GenerationComponent } from './generation.component';
import { GenerationDetailComponent } from './generation-detail/generation-detail.component';
import { BillComponent } from './bill/bill.component';

const routes: Routes = [
  {
    path: '',
    component: GenerationComponent
  },
  { path: ':id', component: GenerationDetailComponent },
  { path: ':id/bill', component: BillComponent },
  { path: ':id/bill/:billId', component: BillComponent },
];

export const GenrationRoutingRoutes = RouterModule.forChild(routes);
