import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { GenrationRoutingRoutes } from './genration-routing.routing';
import { MatModule } from '../mat.module';
import { ChartsModule } from 'ng2-charts';

import { GenerationComponent } from './generation.component';
import { GenerationOverviewDialog } from './generation-overview-dialog/generation.dialog';
import { GenerationDetailComponent } from './generation-detail/generation-detail.component';
import { BillComponent } from './bill/bill.component';
import { BillTotalComponent } from './bill-total/bill-total.component';
import { NgxsModule } from '@ngxs/store';
import { GenerationState } from '../_state/generation.state';
import { CostComponent } from './cost/cost.component';
import { ItemsState } from '../_state/items.state';
import { CostState } from '../_state/cost/cost.state';
import { WeightState } from '../_state/weight/weight.state';
import { BillState } from '../_state/bill/bill.state';
import { BillTotalState } from '../_state/bill/bill-total.state';
import { CatcherState } from '../_state/catcher.state';
import { BillService } from '../_services/bill.service';
import { SharesModule } from '../_shares/shares.module';

@NgModule({
  imports: [
    CommonModule,
    MatModule,
    FormsModule,
    ReactiveFormsModule,
    NgxsModule.forFeature([
      GenerationState,
      ItemsState,
      CostState,
      BillTotalState,
      BillState,
      WeightState,
      CatcherState
    ]),
    GenrationRoutingRoutes,
    ChartsModule,
    SharesModule
  ],
  declarations: [
    GenerationOverviewDialog,
    GenerationComponent,
    GenerationDetailComponent,
    CostComponent,
    BillComponent,
    BillTotalComponent
  ],
  providers: [
    BillService
  ],
  entryComponents: [GenerationOverviewDialog]
})
export class GenerationModule {}
