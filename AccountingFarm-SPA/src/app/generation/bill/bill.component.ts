import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import {
  AddWeightChickenAction,
  AddWeightBasketAction,
  ResetWeightAction,
  EditWeightChickenAction,
  EditWeightBasketAction,
  RemoveWeightChickenAction,
  RemoveWeightBasketAction,
  InitWeightChickenAction,
  InitWeightBasketAction
} from 'src/app/_state/weight/weight.actions';
import { WeightState } from 'src/app/_state/weight/weight.state';
import { Observable } from 'rxjs';
import { startWith, delay, switchMap, map } from 'rxjs/operators';
import { CatcherState } from 'src/app/_state/catcher.state';
import { Catcher } from 'src/app/_models/catcher';
import { FetchCatcher } from 'src/app/_state/catcher.actions';
import { Bill } from 'src/app/_models/bill';
import {
  AddBillAction,
  FetchBill,
  UpdateBillAction,
  ResetBillAction
} from 'src/app/_state/bill/bill.actions';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { BillState } from 'src/app/_state/bill/bill.state';
import { FetchBillTotal } from 'src/app/_state/bill/bill-total.actions';
import { BillService } from 'src/app/_services/bill.service';
import { FetchGenerationReport } from 'src/app/_state/generation.actions';

export interface PeriodicElement {
  zero?: number;
  one?: number;
  two?: number;
  three?: number;
  four?: number;
  five?: number;
  six?: number;
  seven?: number;
  eight?: number;
  nine?: number;
  ten?: number;
}

@Component({
  selector: 'af-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.scss']
})
export class BillComponent implements OnInit, AfterViewInit, OnDestroy {
  @Select(WeightState.getWeightChicken) weightChicken$: Observable<string[]>;
  @Select(WeightState.getWeightBasket) weightBasket$: Observable<string[]>;
  @Select(CatcherState.getCatchers) catcher$: Observable<Catcher[]>;
  @Select(BillState.getBillAll) bill$: Observable<Bill>;

  generationId: string;

  billForm: FormGroup;
  billPrefix: string;
  filteredOptions: Observable<Catcher[]>;
  options: string[] = ['One', 'Two', 'Three'];

  displayedColumns: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'];
  maxRow = 21;
  data: PeriodicElement[];
  weightChicken: number = null;
  weightBasket: number = null;
  indexSelected: number = null;

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private localtion: Location,
    private alertify: AlertifyService,
    private billService: BillService
  ) {}

  ngOnInit() {
    this.store.dispatch(new FetchCatcher());
    this.generationId = this.route.snapshot.params.id;
    this.initForm();
    if (this.route.snapshot.params.billId) {
      this.initFormValue();
    }
    this.getPrefix();
  }

  private getPrefix() {
    this.billService.getBillPrefix(this.generationId).subscribe(res => {
      this.billPrefix = res;
    });
  }

  private initFormValue() {
    this.store.dispatch(new FetchBill(this.route.snapshot.params.billId));
    this.bill$.subscribe(p => {
      if (p) {
        this.billPrefix = p.prefixBill;
        this.billForm.setValue(p);
        this.store.dispatch(new InitWeightChickenAction(p.weightChickens));
        this.store.dispatch(new InitWeightBasketAction(p.weightBaskets));
      }
    });
  }

  initForm() {
    this.billForm = this.fb.group({
      id: [''],
      prefixBill: [''],
      catchDate: [new Date(), Validators.required],
      catcherId: ['', Validators.required],
      catchAmount: ['', Validators.required],
      weightChickens: [''],
      weightBaskets: [''],
      generationId: ['']
    });
  }

  onWeightChickenAdd(val: number) {
    if (val !== null && this.indexSelected === null) {
      this.store.dispatch(new AddWeightChickenAction(val.toString()));
      this.weightChicken = null;
      this.indexSelected = null;
    } else if (val) {
      this.store.dispatch(
        new EditWeightChickenAction(this.indexSelected, val.toString())
      );
      this.weightChicken = null;
      this.indexSelected = null;
      this.ngAfterViewInit();
    }
  }

  onWeightBasketAdd(val: number) {
    if (val !== null && this.indexSelected === null) {
      this.store.dispatch(new AddWeightBasketAction(val.toString()));
      this.weightBasket = null;
    } else if (val) {
      this.store.dispatch(new EditWeightBasketAction(this.indexSelected, val.toString()));
      this.weightBasket = null;
      this.indexSelected = null;
      this.ngAfterViewInit();
    }
  }

  onSaveClick(bill?: Bill) {
    if (this.billForm.valid) {
      bill = this.billForm.value;
      bill.prefixBill = this.billPrefix;

      bill.generationId = this.generationId;
      this.weightChicken$.subscribe(p => {
        bill.weightChickens = p;
      });
      this.weightBasket$.subscribe(p => {
        bill.weightBaskets = p;
      });
      if (bill.id) {
        this.store.dispatch(new UpdateBillAction(bill)).subscribe(
          () => {
            this.alertify.success('แก้ไขสำเร็จ');
          },
          error => {
            this.alertify.error(error);
          },
          () => {
            this.store.dispatch(new FetchBillTotal(this.generationId));
            this.store.dispatch(new FetchGenerationReport(this.generationId));
            this.localtion.back();
          }
        );
      } else {
        this.store.dispatch(new AddBillAction(bill)).subscribe(
          (res) => {
            if (res) {
              this.alertify.success('บันทึกสำเร็จ');
              this.getPrefix();
            }
          },
          error => {
            this.alertify.error(error);
          }
        );
      }
      this.onCancleClick();
    }
  }

  onCancleClick() {
    this.billForm.reset({ catchDate: new Date() });
    this.weightChicken = null;
    this.weightBasket = null;
    if (this.route.snapshot.params.billId) {
      this.initFormValue();
    } else {
      this.ngOnDestroy();
    }
  }

  onEditChickenClick(index: number) {
    this.weightBasket = null;
    this.weightChicken$.pipe(map((stores: string[]) => stores[index])).subscribe(p => {
      if (p) {
        this.weightChicken = +p;
        this.indexSelected = index;
      }
    });
  }

  onEditBasketClick(index: number) {
    this.weightChicken = null;
    this.weightBasket$.pipe(map((stores: string[]) => stores[index])).subscribe(p => {
      if (p) {
        this.weightBasket = +p;
        this.indexSelected = index;
      }
    });
  }

  onDeleteChicken(value: any) {
    this.store.dispatch(new RemoveWeightChickenAction(value));
  }

  onDeleteBasket(value: any) {
    this.store.dispatch(new RemoveWeightBasketAction(value));
  }

  ngAfterViewInit() {
    const dataValue: Array<string[]> = [];
    let a = 0;
    let b = 0;
    this.weightChicken$
      .pipe(
        startWith({}),
        delay(0),
        switchMap(() => {
          return this.weightChicken$;
        }),
        map(data => {
          dataValue[0] = data;
          a = dataValue[0].length;
          return this.getDataRow(a, b, dataValue);
        })
      )
      .subscribe(p => {
        this.data = p;
      });

    this.weightBasket$
      .pipe(
        startWith({}),
        delay(0),
        switchMap(() => {
          return this.weightBasket$;
        }),
        map(p => {
          dataValue[1] = p;
          b = dataValue[1].length;
          return this.getDataRow(a, b, dataValue);
        })
      )
      .subscribe(p => {
        this.data = p;
      });
  }

  private getDataRow(a: number, b: number, data: string[][]) {
    const value: Array<PeriodicElement> = [];
    for (let index = 0; index < this.maxRow; index++) {
      const _value: PeriodicElement = {};
      if (index < a || index < b) {
        _value.zero = +data[0][index];
        _value.one = +data[0][index + this.maxRow];
        _value.two = +data[0][index + this.maxRow * 2];
        _value.three = +data[0][index + this.maxRow * 3];
        _value.four = +data[0][index + this.maxRow * 4];
        _value.five = +data[0][index + this.maxRow * 5];
        _value.ten = +data[0][index + this.maxRow * 6];
        _value.six = data[1] ? +data[1][index] : null;
        _value.seven = data[1] ? +data[1][index + this.maxRow] : null;
        _value.eight = data[1] ? +data[1][index + this.maxRow * 2] : null;
        _value.nine = data[1] ? +data[1][index + this.maxRow * 3] : null;
      } else {
        _value.zero = null;
        _value.one = null;
        _value.two = null;
        _value.three = null;
        _value.four = null;
        _value.five = null;
        _value.ten = null;
        _value.six = null;
        _value.seven = null;
        _value.eight = null;
        _value.nine = null;
      }

      value.push(_value);
    }
    return value;
  }

  ngOnDestroy() {
    this.store.dispatch(new ResetWeightAction());
    this.store.dispatch(new ResetBillAction());
  }
}
